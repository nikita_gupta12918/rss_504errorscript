#!/bin/bash
date=$(date +%m-%d-%Y)
mkdir -p /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date
pid=$(ps -ef | grep -E '(^|\s)/usr/lib/rstudio-server/bin/rserver($|\s)' | grep -i "rstudio+" | awk '{ print $2}') >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
pid_launcher=$(ps -ef | grep -E '(^|\s)/usr/lib/rstudio-server/bin/rstudio-launcher($|\s)' | grep -i "rstudio+" | awk '{ print $2}') >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/
logs
echo "$pid" "$pid_launcher"
sudo echo "Open files for rstudio-server process $(ls -l /proc/$pid/fd|wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo echo "Open files for rstudio-launcher process $(ls -l /proc/$pid_launcher/fd|wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo echo "No of all open files for top 10 processes on Linux operating systems or server $(lsof | awk '{print $1}' | sort | uniq -c | sort -r -n | head)" >> /RSTUDIO_SERVERPRO/automation
/502ErrorScript/$date/logs
sudo echo "No of open connections on 8787 port $(netstat -anp | grep :8787 |wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo echo "No of open connections on rsession  $(netstat -antlp | grep rsession |wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo echo "No of open connections on rsession-laucher port $(netstat -antlp | grep rstudio-launch|wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo echo "No of open connections on 127.0.0.1 $(netstat -antlp|grep 127.0.0.1 |wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo echo  "No of open connection on server IP $(netstat -antlp|grep 10.232.45.108 |wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo echo  "No of open connection in CLOSE_WAIT state $(netstat -antlp |grep CLOSE_WAIT | wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo echo "No of open connection in TIME_WAIT $(netstat -antlp |grep TIME_WAIT | wc -l)" >> /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/logs
sudo timeout 300 sudo strace -o /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/output1.txt -t -p $pid
sudo timeout 300 sudo strace -o /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/output2.txt -c -p $pid
sudo timeout 300 sudo strace -o /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/output3.txt -t -p $pid_launcher
sudo timeout 300 sudo strace -o /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/output4.txt -c -p $pid_launcher
sudo timeout 180 sudo strace -o /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/output5_child_processdetails.txt -t -fp $pid
sudo timeout 180 sudo strace -o /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/output6_child_processdetails.txt -t -fp $pid
sudo timeout 180 sudo strace -o /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/output7_child_processdetails.txt -c -fp $pid_launcher
sudo timeout 180 sudo sudo strace -o /RSTUDIO_SERVERPRO/automation/502ErrorScript/$date/output8_childprocessdetails.txt -c -fp $pid_launcher